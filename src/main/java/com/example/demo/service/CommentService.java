package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	//レコードの追加
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	//レコードの全件取得（更新日時の降順）
	public List<Comment> findAllComment(){
		return commentRepository.findAllByOrderByUpdatedDateDesc();
	}

	//レコード1件取得
	public Comment findComment(Integer id) {
		Comment comment = (Comment) commentRepository.findById(id).orElse(null);
		return comment;
	}

	//レコードの更新
	public void updateComment(Comment comment) {
		commentRepository.save(comment);
	}

	//レコードの削除
	public void deleteComment(Integer id) {
		Comment comment = commentRepository.findById(id).get();
		commentRepository.delete(comment);
	}


}
