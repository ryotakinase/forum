package com.example.demo.service;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport() {
		//更新日時の降順で取得
		return reportRepository.findAllByOrderByUpdatedDateDesc();
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	//レコード1件の削除
	public void deleteReport(Integer id) {
		Report report = reportRepository.findById(id).get();
		reportRepository.delete(report);
	}

	//レコード1件取得
	public Report findReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	//レコードの更新
	public void updateReport(Report report) {
		reportRepository.save(report);
	}

	//レコードの部分取得（日時）
	public List<Report> findByCreatedDateBetween(String startDate, String endDate) {

		Timestamp start = null;
	    if(!startDate.isEmpty()) {
	    	start = Timestamp.valueOf(startDate + " 00:00:00");
	    } else {
	    	start = Timestamp.valueOf("2020-01-01 00:00:00");
	    }

	    Timestamp end = null;
	    if(!endDate.isEmpty()) {
	    	end = Timestamp.valueOf(endDate + " 23:59:59");
	    } else {
	    	end = new Timestamp(System.currentTimeMillis());
	    }

	    return reportRepository.findByCreatedDateBetweenOrderByUpdatedDateDesc(start, end);
	}
}
