package com.example.demo.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {

	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top(@RequestParam(required=false, name="startDate") String startDate, @RequestParam(required=false, name="endDate") String endDate) {
		ModelAndView mav = new ModelAndView();

		//入力した日付を残す
		if(startDate != null) {
			mav.addObject("startDate", startDate);
		}
		if(endDate != null) {
			mav.addObject("endDate", endDate);
		}

		List<Report> contentData;

		//日付の有無によって全件取得か一部取得か
		if(startDate == null && endDate == null) {
			contentData = reportService.findAllReport();
		} else {
			contentData = reportService.findByCreatedDateBetween(startDate, endDate);
		}

		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		//コメントの取得
		List<Comment> commentData = commentService.findAllComment();
		//コメント投稿用の空のオブジェクト
		Comment comment = new Comment();
		mav.addObject("commentModel", comment);
		mav.addObject("comments", commentData);
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		// 投稿をテーブルに格納
		reportService.saveReport(report);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//投稿削除機能
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		reportService.deleteReport(id);
		return new ModelAndView("redirect:/");
	}

	//編集画面
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Report report = reportService.findReport(id);
		mav.addObject("formModel", report);
		mav.setViewName("/edit");
		return mav;
	}

	//編集処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		report.setId(id);
		//投稿の作成日時を更新時に再度設定する
		Date createdDate = reportService.findReport(id).getCreatedDate();
		report.setCreatedDate(createdDate);
		reportService.updateReport(report);
		return new ModelAndView("redirect:/");
	}

	//コメント投稿処理
	@PostMapping("/addComment/{id}")
	public ModelAndView addComent(@PathVariable Integer id, @ModelAttribute("commentModel") Comment comment) {
		comment.setReportId(id);
		commentService.saveComment(comment);

		Report report = reportService.findReport(id);
		report.setUpdatedDate(new Date());
		reportService.updateReport(report);

		return new ModelAndView("redirect:/");
	}

	//コメント編集画面
	@GetMapping("/editComment/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Comment comment = commentService.findComment(id);
		mav.addObject("commentModel", comment);
		mav.setViewName("/editComment");
		return mav;
	}

	//コメント更新処理
	@PutMapping("/updateComment/{id}")
	public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("commentModel") Comment comment) {
		comment.setId(id);
		//コメントの作成日時を再度設定する
		Date createdDate = commentService.findComment(id).getCreatedDate();
		comment.setCreatedDate(createdDate);
		commentService.updateComment(comment);
		//コメントに紐づく投稿の更新日時を変更
		int reportId = commentService.findComment(id).getReportId();
		Report report = reportService.findReport(reportId);
		report.setUpdatedDate(new Date());
		reportService.updateReport(report);
		return new ModelAndView("redirect:/");
	}

	//コメント削除処理
	@DeleteMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}

}
